package com.example.arbitrosfabmalaga;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Pistas extends Activity {	//Clase para cargar las pistas
	ListView listPistas;	//Lista de pistas
	Button btnBuscaPista;	//Boton para buscar las pistas
	String coordenadas;	//Variable para las coordenadas de la pista
	String nombre;	//Variable para el nombre de la pista
	String latitud="";	//Variable para la latitud
	String longitud="";	//Variable para la altitud
	private String result=null;	//Variable para el resultado de la consulta
	private InputStream is=null;	//Variable para el lector
	private StringBuilder sb=null; 	//Variable para convertir a cadena
	@Override
	protected void onCreate(Bundle savedInstanceState) {	//M�todo para la creaci�n de la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Crea la instancia
		setContentView(R.layout.activity_pistas);	//Asigna el layout de la activity
		listPistas=(ListView)findViewById(R.id.listPistas);	//Referencia la lista de pistas
		btnBuscaPista=(Button)findViewById(R.id.btnBuscaPista);	//Referencia el bot�n de b�squeda
		btnBuscaPista.setPadding(10, 10, 10, 10);	//Asigna padding al bot�n
		TareaPistas tarea=new TareaPistas();	//Crea un objeto del tipo tareapistas
		tarea.cargaContenido(getApplicationContext());	//Carga el contexto
		tarea.onPreExecute();	//Llama al m�todo previo a la ejecuci�n en background
		tarea.execute(listPistas);	//Ejecuta la clase tareapistas
		listPistas.setOnItemClickListener(new OnItemClickListener(){	//Evento onclick en elemento de la lista
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position, long id){	//M�todo para el evento onclick
		        nombre=((Pabellon)a.getAdapter().getItem(position)).getNombre();	//Obtiene el elemento seleccionado de la lista
		        btnBuscaPista.setOnClickListener(new OnClickListener(){	//Evento onclick del boton
		        	@Override
					public void onClick(View v){	//M�todo del evento onclick
		        		new asyncpistas().execute(nombre);	//Ejecuta la clase asyncpistas
		        	}
		        });
			}
		});
	}
	static class TareaPistas extends AsyncTask<ListView, Void, ArrayAdapter<Pabellon>>{	//Clase asincrona para obtener la lista de pistas
		Context contexto;	//Contexto
		ListView listPistas;	//Lista de pistas
		InputStream is;	//Leer los resultados
		ArrayList<Pabellon> lista=new ArrayList<Pabellon>();	//Arraylist del tipo Pabellon
		public void cargaContenido(Context contexto){	//Constructor
			this.contexto=contexto;
		}
		@Override
		protected void onPreExecute(){	//M�todo previo a la ejecuci�n en background
			
		}
		@Override
		protected ArrayAdapter<Pabellon> doInBackground(ListView... params){	//M�todo para realizar la consulta en background
			listPistas=params[0];	//Lista
			String resultado="error";	//Variable para almacenar el resultado de la consulta
			Pabellon pab;	//Variable del tipo Pabellon
			HttpClient cliente=new DefaultHttpClient();	//Cliente HTTP
			HttpGet peticion=new HttpGet("http://www.abelsuviri.tk/android/pabellones.php");	//Direcci�n del script PHP a sincronizar
			try{
				HttpResponse response=cliente.execute(peticion);	//Ejecuta la petici�n al servidor
				HttpEntity contenido=response.getEntity();	//Obtiene la respuesta del servidor
				is=contenido.getContent();	//Lee el contenido de la respuesta
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			BufferedReader br=new BufferedReader(new InputStreamReader(is));	//Lee el buffer
			StringBuilder sb=new StringBuilder();	//Convierte a cadena
			String linea=null;	//Variable para las lineas leidas
			try{
				while((linea=br.readLine())!=null){	//Mientras haya lineas sigue leyendo
					sb.append(linea);
				}
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			try{
				is.close();	 //Cierra el lector
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			resultado=sb.toString();	//Almacena los datos leidos en la variable resultado
			try{
				JSONArray jArray=new JSONArray(resultado);	//Array JSON
				for(int i=0;i<jArray.length();i++){
					JSONObject objetoJSON=jArray.getJSONObject(i);	//Objeto JSON
					pab=new Pabellon(objetoJSON.getString("Nombre"));	//Almacena en la variable pab los nombres devueltos de la base de datos
					lista.add(pab);	//A�ade las pistas a la lista
				}
			} catch(JSONException e){
			}
			ArrayAdapter<Pabellon> adaptador=new ArrayAdapter<Pabellon>(contexto, android.R.layout.simple_list_item_1, lista);	//Adaptador de la lista
			return adaptador;
		}
		@Override
		protected void onPostExecute(ArrayAdapter<Pabellon> result){	//M�todo posterior a la ejecuci�n de la tarea en background
			listPistas.setAdapter(result);	//Asigna el adaptador
		}
		@Override
		protected void onProgressUpdate(Void... values){
			
		}
	}
	class asyncpistas extends AsyncTask<String, String, String>{	//Clase asincrona para cargar el mapa
		@Override
		protected String doInBackground(String... params){	//Metodo en background
			try{
				HttpClient httpclient=new DefaultHttpClient();	//Cliente HTTP
				HttpPost httppost=new HttpPost("http://proyectoabel.tk/android/pistas.php?nombre="+URLEncoder.encode(nombre, "UTF-8"));	//Direcci�n del script PHP
				HttpResponse response=httpclient.execute(httppost);	//Respuesta del servidor
				HttpEntity entity=response.getEntity();	//Obtiene la respuesta del servidor
				is=entity.getContent();	//Obtiene el contenido de la respuesta
			} catch(Exception e){	
				Log.e("Conexion: ",e.toString());
				Toast.makeText(getApplicationContext(), "Conexi�n fallida", Toast.LENGTH_LONG).show();
			}
			try{
				BufferedReader reader=new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);	//Lector
				sb=new StringBuilder();	//Crea cadenas
				sb.append(reader.readLine()+"\n");
				String line="0";
				while((line=reader.readLine())!=null){	//Lee lineas hasta que no haya m�s
					sb.append(line+"\n");	//Convierte a cadena las lineas
				}
				is.close();	//Cierra el lector
				result=sb.toString();	//Guarda el resultado de las lineas
			} catch(Exception e){
				Log.e("Error: ", e.toString());
			}
			try{
				JSONArray jArray=new JSONArray(result);	//Crea un array JSON
				for(int i=0;i<jArray.length();i++){
					JSONObject json_data=jArray.getJSONObject(i);	//Lee los datos del array
					latitud=String.valueOf(json_data.getString("latitud"));	//Obtiene la latitud obtenida de la base de datos
					longitud=String.valueOf(json_data.getString("longitud"));	//Obtiene la longitud obtenida de la base de datos
				}
				coordenadas=latitud+", "+longitud;	//Variable con las coordenadas concatenadas
				Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr="+coordenadas));	//Crea un lanzador de la aplicaci�n maps
				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");	//Selecciona la aplicaci�n maps
				startActivity(intent);	//Inicia Google Maps
			} catch(JSONException e){
				Log.e("Error: ", e.toString());
			}
			return coordenadas;
		}
	}
}
