package com.example.arbitrosfabmalaga;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Nomina extends Activity {	//Clase para mostrar la n�mina

	@Override
	protected void onCreate(Bundle savedInstanceState) {	//M�todo para la creaci�n de la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Crea la instancia
		setContentView(R.layout.activity_nomina);	//Asigna el layout de la activity
		Bundle bundle = this.getIntent().getExtras();	//Obtiene las variables del lanzador
		final String usuario=bundle.getString("usuario");	//Obtiene la variable usuario de la activity anterior
		final String direccion="http://abelsuviri.tk/android/nomina.php?usuario="+usuario;	//Direccion de la pagina PHP a cargar
		WebView wb1=(WebView)findViewById(R.id.wbVerNomina);	//Referencia el webview
		wb1.getSettings().setJavaScriptEnabled(true);	//Activa JavaScript en el webview
		wb1.setWebViewClient(new Callback2());	//Crea el cliente web
		wb1.loadUrl("http://docs.google.com/gview?embedded=true&url="+direccion);	//Abre el script PHP en el webview
	}
	private class Callback2 extends WebViewClient {	//Clase para seleccionar el cliente web
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }
}
