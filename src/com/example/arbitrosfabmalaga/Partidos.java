package com.example.arbitrosfabmalaga;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Partidos extends Activity {	//Clase para seleccionar la localidad de los partidos a consultar
	String opcion;	//Variable para la opcion seleccionada
	ListView ListLocalidad;	//Lista de localidades
	Button btnBuscar;	//Boton buscar
	ArrayList<Localidad> lista=new ArrayList<Localidad>();	//Arraylist de tipo localidad
	Localidad loc;	//Variable de tipo localidad
	
	@Override
	public void onCreate(Bundle savedInstanceState) {	//Metodo para la creaci�n de la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Asigna la instancia
		setContentView(R.layout.activity_partidos);	//Selecciona el layout de la activity
		ListLocalidad=(ListView)findViewById(R.id.ListLocalidad);	//Referencia la lista
		btnBuscar=(Button)findViewById(R.id.btnBuscar);	//Referencia el bot�n
		btnBuscar.setPadding(10, 10, 10, 10);	//Asigna padding al bot�n
		Tarea tarea=new Tarea();	//Crea un nuevo objeto de la clase tarea
		tarea.cargaContenido(getApplicationContext());	//Crea el objeto usando el constructor
		tarea.onPreExecute();	//Ejecuta el m�todo para ejecutar la consulta en la base de datos
		tarea.execute(ListLocalidad);	//Ejecuta la clase tarea
		ListLocalidad.setOnItemClickListener(new OnItemClickListener(){	//Evento onclick en un elemento de la lista
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position, long id){	//M�todo del evento onclick
				opcion=((Localidad)a.getAdapter().getItem(position)).getNombre();	//Obtiene la opci�n seleccionada de la lista
			}
		});
		btnBuscar.setOnClickListener(new OnClickListener(){	//Evento onclick del boton
			@Override
			public void onClick(View v){	//M�todo del evento onclick
				Intent i=new Intent(Partidos.this, Partidos2.class);	//Crea un nuevo lanzador de la activity Partidos2
				i.putExtra("localidad", opcion);	//Pasa como variable la opci�n seleccionada
				startActivity(i);	//Inicia la activity Partidos2
			}
		});
	}
	static class Tarea extends AsyncTask<ListView, Void, ArrayAdapter<Localidad>>{	//Clase asincrona para ejecutar la consulta en la base de datos
		Context contexto;	//Contexto
		ListView ListaLocalidad;	//Lista de localidades
		InputStream is;	
		ArrayList<Localidad> lista=new ArrayList<Localidad>();	//Arraylist de localidades
		public void cargaContenido(Context contexto){	//Constructor
			this.contexto=contexto;
		}
		@Override
		protected void onPreExecute(){	//M�todo para antes de la ejecuci�n
			
		}
		@Override
		protected ArrayAdapter<Localidad> doInBackground(ListView... params){	//M�todo para ejecutar la consulta en background
			ListaLocalidad=params[0];	//Obtiene la lista de localidad
			String resultado="error";	//Variable para el resultado de la consulta
			Localidad loc;	//Variable para la localidad
			HttpClient cliente=new DefaultHttpClient();	//Nuevo objeto cliente HTTP
			HttpGet peticion=new HttpGet("http://www.abelsuviri.tk/android/localidad.php");	//Direcci�n del script PHP
			try{
				HttpResponse response=cliente.execute(peticion);	//Ejecuta la petici�n al servidor
				HttpEntity contenido=response.getEntity();	//Procesa la respuesta
				is=contenido.getContent();	//Obtiene el contenido del script
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			BufferedReader br=new BufferedReader(new InputStreamReader(is));	//Lee el contenido devuelto por el script PHP
			StringBuilder sb=new StringBuilder();	//Crea cadenas
			String linea=null;	//Variable para leer las lineas
			try{
				while((linea=br.readLine())!=null){	//Mientras haya lineas sigue leyendo y guardandolas en la variable
					sb.append(linea);
				}
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			try{
				is.close();	//Cierra el lector
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			resultado=sb.toString();	//Convierte a cadena el resultado
			try{
				JSONArray jArray=new JSONArray(resultado);	//Crea un array JSON
				for(int i=0;i<jArray.length();i++){	
					JSONObject objetoJSON=jArray.getJSONObject(i);	//Obtiene las filas de la consulta
					loc=new Localidad(objetoJSON.getString("Nombre"));	//Guarda el nombre de la localidad en una variable
					lista.add(loc);	//A�ade la variable a la lista
				}
			} catch(JSONException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			ArrayAdapter<Localidad> adaptador=new ArrayAdapter<Localidad>(contexto, android.R.layout.simple_list_item_1, lista);	//Adaptador de la lista
			return adaptador;
		}
		@Override
		protected void onPostExecute(ArrayAdapter<Localidad> result){
			ListaLocalidad.setAdapter(result);
		}
		@Override
		protected void onProgressUpdate(Void... values){
			
		}
	}
}
