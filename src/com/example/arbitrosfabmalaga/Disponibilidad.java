package com.example.arbitrosfabmalaga;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Disponibilidad extends Activity {	//Clase principal de la activity
	//Variables para asignar el valor de los checkbox
	String v;
	String sm;
	String st;
	String dm;
	String dt;
	String l;
	String m;
	String x;
	String j;
	String c;
	String mo;
	String usuario;	//Variable para el usuario
	String observaciones;	//Variable para las observaciones
	EditText txtObservaciones;	//Entrada de texto de observaciones
	@Override
	protected void onCreate(Bundle savedInstanceState) {	//M�todo para crear la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_disponibilidad);	//Asigna el layout de la activity
		Bundle bundle = this.getIntent().getExtras();	//Obtiene las variables pasadas de la activity anterior
		usuario=bundle.getString("usuario");	//Obtiene la variable usuario
		//Referencia los checkbox
		final CheckBox chV=(CheckBox)findViewById(R.id.chV);
		final CheckBox chSM=(CheckBox)findViewById(R.id.chSM);
		final CheckBox chST=(CheckBox)findViewById(R.id.chST);
		final CheckBox chDM=(CheckBox)findViewById(R.id.chDM);
		final CheckBox chDT=(CheckBox)findViewById(R.id.chDT);
		final CheckBox chL=(CheckBox)findViewById(R.id.chL);
		final CheckBox chM=(CheckBox)findViewById(R.id.chM);
		final CheckBox chX=(CheckBox)findViewById(R.id.chX);
		final CheckBox chJ=(CheckBox)findViewById(R.id.chJ);
		final CheckBox chC=(CheckBox)findViewById(R.id.chC);
		final CheckBox chMo=(CheckBox)findViewById(R.id.chMo);
		txtObservaciones=(EditText)findViewById(R.id.txtObservaciones); //Referencia la entrada de texto de observaciones
		Button btnEnviarDisponibilidad=(Button)findViewById(R.id.btnEnviarDisponibilidad);	//Referencia el bot�n enviar
		btnEnviarDisponibilidad.setPadding(10, 10, 10, 10);
		btnEnviarDisponibilidad.setOnClickListener(new OnClickListener(){	//Evento onclick del bot�n
			@Override
			public void onClick(View v1){	//M�todo para el click
				//Comprueba si los checkbox est�n marcados y asigna a las variables los valores correspondientes
				if(chV.isChecked()==true){
					v="Si";
				}
				else{
					v="No";
				}
				if(chSM.isChecked()==true){
					sm="Si";
				}
				else{
					sm="No";
				}
				if(chST.isChecked()==true){
					st="Si";
				}
				else{
					st="No";
				}
				if(chDM.isChecked()==true){
					dm="Si";
				}
				else{
					dm="No";
				}
				if(chDT.isChecked()==true){
					dt="Si";
				}
				else{
					dt="No";
				}
				if(chL.isChecked()==true){
					l="Si";
				}
				else{
					l="No";
				}
				if(chM.isChecked()==true){
					m="Si";
				}
				else{
					m="No";
				}
				if(chX.isChecked()==true){
					x="Si";
				}
				else{
					x="No";
				}
				if(chJ.isChecked()==true){
					j="Si";
				}
				else{
					j="No";
				}
				if(chC.isChecked()==true){
					c="Si";
				}
				else{
					c="No";
				}
				if(chMo.isChecked()==true){
					mo="Si";
				}
				else{
					mo="No";
				}
				new asyncinsert().execute();	//Crea un nuevo objeto de la clase asyncinsert y lo ejecuta
			}
				
		});
	}
	class asyncinsert extends AsyncTask<String, String, String>{	//Clase asincrona para insertar en la base de datos
		@Override
		protected String doInBackground(String... params){	//M�todo para ejecutar en background
			String ok="ok";	//Variable que devuelve, ya que el m�todo debe devolver una cadena de forma obligatoria
			HttpClient httpclient=new DefaultHttpClient();	//Crea el cliente HTTP
			HttpPost httppost=new HttpPost("http://abelsuviri.tk/android/disponibilidad.php");	//Asigna el script PHP a ejecutar
			try{
				List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(12);	//Arraylist para los valores que se enviaran al PHP
				//Se asignan las variables que se insertar�n al array
				nameValuePairs.add(new BasicNameValuePair("usuario", usuario));
				nameValuePairs.add(new BasicNameValuePair("v", v));
				nameValuePairs.add(new BasicNameValuePair("sm", sm));
				nameValuePairs.add(new BasicNameValuePair("st", st));
				nameValuePairs.add(new BasicNameValuePair("dm", dm));
				nameValuePairs.add(new BasicNameValuePair("dt", dt));
				nameValuePairs.add(new BasicNameValuePair("l", l));
				nameValuePairs.add(new BasicNameValuePair("m", m));
				nameValuePairs.add(new BasicNameValuePair("x", x));
				nameValuePairs.add(new BasicNameValuePair("j", j));
				nameValuePairs.add(new BasicNameValuePair("c", c));
				nameValuePairs.add(new BasicNameValuePair("mo", mo));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpclient.execute(httppost);	//Ejecuta el script PHP
				String[] para={"abelsuviri@gmail.com"};	//Variable para el destinatario del email
				String mensaje="El �rbitro "+usuario+" ha modificado su disponibilidad. Observaciones: "+txtObservaciones.getText().toString();	//Mensaje del email
				Intent email=new Intent(Intent.ACTION_SEND);	//Crea el lanzador
				email.putExtra(Intent.EXTRA_EMAIL, para);	//Asigna el destinatario
				email.putExtra(Intent.EXTRA_SUBJECT, "Modificaci�n de disponibilidad");	//Asigna el asunto
				email.putExtra(Intent.EXTRA_TEXT, mensaje);	//Asigna el mensaje
				email.setType("message/rfc822");	//Asigna el charset
				startActivity(Intent.createChooser(email, "Email "));	//Abre el cliente de correo electr�nico
			} catch(Exception e){	//Captura excepci�n
				Toast.makeText(getApplicationContext(), "Conexi�n fallida", Toast.LENGTH_SHORT).show();	//Muestra un mensaje si hay alg�n error
			}
			return ok;
		}
	}
}
