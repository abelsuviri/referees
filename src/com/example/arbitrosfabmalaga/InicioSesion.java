package com.example.arbitrosfabmalaga;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.arbitrosfabmalaga.Httppostaux;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InicioSesion extends Activity {
	private EditText txtUsuario;	//Variable de la entrada de texto de usuario
	private EditText txtClave;	//Variable de la entrada de texto de clave
	private Button btnIniciar;	//Variable del bot�n iniciar sesi�n
	Httppostaux post;	//Objeto del tipo Httppostaux
	String url="http://abelsuviri.tk/android/acces.php";	//Direcci�n del script PHP
	boolean result_back;
	private ProgressDialog pDialog;	//Variable para el dialogo de carga
	@Override
	public void onCreate(Bundle savedInstanceState) {	//M�todo para crear la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Crea la instancia
		setContentView(R.layout.activity_inicio_sesion);	//Asigna el layout de la activity
		post=new Httppostaux();	//Crea un nuevo objeto de la clase Httppostaux
		txtUsuario=(EditText)findViewById(R.id.txtUsuario);	//Referencia la entrada de texto de usuario
		txtClave=(EditText)findViewById(R.id.txtClave);	//Referencia la entrada de texto de clave
		btnIniciar=(Button)findViewById(R.id.btnIniciar);	//Referencia el bot�n de iniciar sesi�n
		btnIniciar.setOnClickListener(new View.OnClickListener(){	//Evento onclick del bot�n
			@Override
			public void onClick(View v){	//M�todo del onclick
				String usuario=txtUsuario.getText().toString();	//Convierte a cadena en una variable el valor de la entrada de texto de usuario
				String clave=txtClave.getText().toString();	//Convierte a cadena en una variable el valor de la entrada de texto de clave
				if(checklogindata(usuario, clave)==true){	//Si se introdujeron datos ejecuta la clase asynclogin
					new asynclogin().execute(usuario, clave);
				}
				else{
					err_login();
				}
			}
		});
	}
	public void err_login(){	//M�todo para mostrar un error si no se introdujeron usuario y/o contrase�a
		Vibrator vibrador=(Vibrator)getSystemService(Context.VIBRATOR_SERVICE);	//Crea un nuevo objeto vibrador
		vibrador.vibrate(200);	//Ejecuta el vibrador del tel�fono
		Toast.makeText(getApplicationContext(), "Usuario o contrase�a incorrectos", Toast.LENGTH_LONG).show();	//Muesta un mensaje de error
	}
	public boolean loginstatus(String usuario, String clave){	//M�todo para comprobar si el usuario y contrase�a son correctos
		int logstatus=-1;
		ArrayList<NameValuePair> postparameters2send=new ArrayList<NameValuePair>();	//Crea un arraylist para enviar las variables al script PHP
		postparameters2send.add(new BasicNameValuePair("usuario", usuario));	//Agrega la variable usuario
		postparameters2send.add(new BasicNameValuePair("clave", clave));	//Agrega la variable clave
		JSONArray jdata=post.getserverdata(postparameters2send, url);	//Envia las variables
		if(jdata!=null && jdata.length()>0){ 	//Comprueba que haya datos en el array
			JSONObject json_data;	//Crea un objeto JSON
			try{
				//Obtiene los datos devueltos del script
				json_data=jdata.getJSONObject(0);
				logstatus=json_data.getInt("logstatus");	
			} catch(JSONException e){
				e.printStackTrace();
			}
			if(logstatus==0){	//Si se ha devuelto 0 el login no es correcto, si se devuelve 1 es correcto
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return false;
		}
	}
	public boolean checklogindata(String usuario, String clave){	//M�todo para comprobar si se introdujeron usuario y/o contrase�a
		if(usuario.equals("")|| clave.equals("")){	//Si el usuario y/o contrase�a est�n vacios devuelve falso
			return false;
		}
		else{
			return true;
		}
	}
	class asynclogin extends AsyncTask<String, String, String>{	//Clase asincrona para el login
		String usuario;	//Variable para el usuario
		String clave;	//Variable para la clave
		@Override
		protected void onPreExecute(){	//M�todo anterior  la ejecuci�n
			pDialog=new ProgressDialog(InicioSesion.this);	//Crea el dialogo de carga
			pDialog.setMessage("Iniciando sesi�n...");	//Asigna texto al dialogo
			pDialog.setIndeterminate(false);	//Asigna que no sea indefinido
			pDialog.setCancelable(false);	//Asigna que no sea cancelable
			pDialog.show();	//Ejecuta el dialogo
		}
		@Override
		protected String doInBackground(String... params){	//M�todo para ejecutar en background
			usuario=params[0];	//Asigna a usuario el par�metro 0 
			clave=params[1];	//Asigna a clave el par�metro 1
			if(loginstatus(usuario, clave)==true){	//Comprueba que sean correctos el usuario y la clave
				return "ok";
			}
			else{
				return "error";
			}
		}
		@Override
		protected void onPostExecute(String result){	//M�todo para despu�s de la ejecuci�n
			pDialog.dismiss();	//Cierra el dialogo
			if(result.equals("ok")){	//Si el resultado es ok Inicia la siguiente activity y le pasa la variable usuario
				Intent i=new Intent(InicioSesion.this, SesionIniciada.class);
				i.putExtra("usuario", usuario);
				startActivity(i);
			}
			else{
				Toast.makeText(getApplicationContext(), "Login incorrecto", Toast.LENGTH_LONG).show();
			}
		}
	}
}
