package com.example.arbitrosfabmalaga;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class SesionIniciada extends Activity {	//Clase para la zona privada

	@Override
	protected void onCreate(Bundle savedInstanceState) {	//M�todo para crear la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Crea la instancia
		setContentView(R.layout.activity_sesion_iniciada);	//Asigna el layout de la activity
		Bundle bundle = this.getIntent().getExtras();	//Obtiene variables de la activity anterior
		final String usuario=bundle.getString("usuario");	//Obtiene la variable usuario
		//Referencia los botones
		final Button btnDesignacion=(Button)findViewById(R.id.btnDesignacion);	
		final Button btnDisponibilidad=(Button)findViewById(R.id.btnDisponibilidad);
		final Button btnNomina=(Button)findViewById(R.id.btnNomina);
		final Button btnTelefonos=(Button)findViewById(R.id.btnTelefonos);
		btnDesignacion.setOnClickListener(new OnClickListener(){	//Evento onclick del bot�n designacion
			@Override
			public void onClick(View v){	//M�todo del evento onclick
				Intent i=new Intent(SesionIniciada.this, Designacion.class);	//Crea un lanzador para abrir la activity designacion
				i.putExtra("usuario", usuario);	//Envia la variable usuario
				startActivity(i);	//Inicia la activity designacion
			}
		});
		btnDisponibilidad.setOnClickListener(new OnClickListener(){	//Evento onclick del bot�n disponibilidad
			@Override
			public void onClick(View v){	//M�todo del evento onclick
				Intent i1=new Intent(SesionIniciada.this, Disponibilidad.class);	//Crea un lanzador para abrir la activity disponibilidad
				i1.putExtra("usuario", usuario);	//Envia la variable usuario
				startActivity(i1);	//Inicia la activity disponibilidad
			}
		});
		btnNomina.setOnClickListener(new OnClickListener(){	//Evento onclick del bot�n nomina
			@Override
			public void onClick(View v){	//M�todo del evento onclick
				Intent i2=new Intent(SesionIniciada.this, Nomina.class);	//Crea un lanzador para abrir la activity nomina
				i2.putExtra("usuario", usuario);	//Envia la variable usuario
				startActivity(i2);	//Inicia la activity nomina
			}
		});
		btnTelefonos.setOnClickListener(new OnClickListener(){	//Evento onclick del bot�n telefonos
			@Override
			public void onClick(View v){	//M�todo del evento onclick
				Intent i3=new Intent(SesionIniciada.this, Telefonos.class);	//Crea un lanzador para abrir la activity telefonos
				i3.putExtra("usuario", usuario);
				startActivity(i3);	//Inicia la activity telefonos
			}
		});
	}
}
