package com.example.arbitrosfabmalaga;

public class Pabellon {	//Clase para cargar la lista de pistas
	private String nombre;	//Variable para el nombre del �rbitro
	public Pabellon(String nombre){	//Constructor de la clase
		super();	
		this.nombre=nombre;
	}
	public String getNombre(){	//M�todo para obtener el nombre
		return nombre;
	}
	@Override
	public String toString(){	//M�todo que convierte a cadena
		return this.nombre;
	}
}
