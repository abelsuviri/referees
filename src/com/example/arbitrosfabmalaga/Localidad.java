package com.example.arbitrosfabmalaga;

public class Localidad {	//Clase para cargar la lista de localidades de la activity Pistas
	private String nombre;	//Variable para el nombre de la localidad
	public Localidad(String nombre){	//Constructor de la clase
		super();
		this.nombre=nombre;
	}
	public String getNombre(){	//M�todo para obtener el nombre
		return nombre;
	}
	@Override
	public String toString(){	//M�todo para convertir a cadena
		return this.nombre;
	}
}
