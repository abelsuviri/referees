package com.example.arbitrosfabmalaga;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;










//import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TableRow.LayoutParams;

public class Partidos2 extends Activity {	//Clase para cargar los partidos
	private String result=null;	//Variable para el resultado
	private InputStream is=null;	//Variable para el lector
	private StringBuilder sb=null; 	//Variable para el conversor a cadena
	private ProgressDialog pDialog;	//Variable para el di�logo de progreso
	Bundle bundle;	//Variable para obtener variables del la activity anterior 
	String localidad;	//Variable para la localidad
	@Override
	public void onCreate(Bundle savedInstanceState) {	//M�todo para la creaci�n de la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Crea la instancia
		setContentView(R.layout.activity_partidos2);	//Asigna el layout de la activity
		bundle=this.getIntent().getExtras();	//Obtiene las variables de la activity anterior
		new async().execute();	//Ejecuta la clase async
		
	}
	class async extends AsyncTask<String, String, String>{	//Clase async para ejecutar la consulta de los partidos
		@Override
		protected void onPreExecute(){	//M�todo para antes de la ejecuci�n
			pDialog=new ProgressDialog(Partidos2.this);	//Crea el di�logo de progreso
			pDialog.setMessage("Cargando...");	//Asigna el texto del di�logo
			pDialog.setIndeterminate(false);	//El di�logo no ser� indeterminado
			pDialog.setCancelable(false);	//El di�logo no podr� cancelarse
			pDialog.show();	//Muestra el di�logo
		}
		@Override
		protected String doInBackground(String...params){	//M�otodo para ejecutar en background
			try{
				String locl=bundle.getString("localidad");	//Obtiene la localidad de la activity anterior
				HttpClient httpclient=new DefaultHttpClient();	//Asigna el cliente HTTP
				HttpPost httppost=new HttpPost("http://abelsuviri.tk/android/partidos.php?localidad="+URLEncoder.encode(locl, "UTF-8"));	//Direcci�n del script PHP
				HttpResponse response=httpclient.execute(httppost);	//Respuesta del servidor
				HttpEntity entity=response.getEntity();	//Entidad del servidor
				is=entity.getContent();	//Obtiene el contenido
			} catch(Exception e){
				Log.e("Conexion: ",e.toString());
				Toast.makeText(getApplicationContext(), "Conexi�n fallida", Toast.LENGTH_SHORT).show();
			}
			try{
				BufferedReader reader=new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);	//Objeto para leer los datos recibidos del PHP
				sb=new StringBuilder();	//Objeto para crear cadenas
				sb.append(reader.readLine()+"\n");	//Convierte a cadena las lineas leidas
				String linea="0";	//Variable para la linea
				while((linea=reader.readLine())!=null){	//Mientras haya lineas las va almacenando
					sb.append(linea+"\n");
				}
				is.close();	//Cierra el lector
				result=sb.toString();	//Asigna el resultado de la consulta
				return result;	//Devuelve el resultado
			} catch(Exception e){
				Log.e("Error: ", e.toString());
				return result;
			}
		}
		@Override
		protected void onPostExecute(String result){	//M�otodo para ejecutar despu�s de la tarea en background
			try{
				JSONArray jArray=new JSONArray(result);	//Crea un array JSON
				TableLayout tv=(TableLayout)findViewById(R.id.maintable);	//Referencia la tabla donde se insertar�n los datos
				tv.removeAllViewsInLayout();	//Elimina el contenido de la tabla
				int contador=1;
				for(int i=-1;i<jArray.length();i++){	//Bucle para imprimir los datos
					TableRow tr=new TableRow(Partidos2.this);	//Crea una nueva fila
					tr.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));	//Asigna los parametros de la fila
					if(contador==1){	//Si el contador es 1 imprime la cabecera de la tabla
						TextView ea=new TextView(Partidos2.this);
						ea.setText("Local");
						ea.setGravity(Gravity.CENTER);
						ea.setTextColor(Color.WHITE);
						ea.setTypeface(null, Typeface.BOLD);
						tr.addView(ea);
						TextView eb=new TextView(Partidos2.this);
						eb.setText("Visitante");
						eb.setGravity(Gravity.CENTER);
						eb.setTextColor(Color.WHITE);
						eb.setTypeface(null, Typeface.BOLD);
						tr.addView(eb);
						TextView fe=new TextView(Partidos2.this);
						fe.setText("Fecha");
						fe.setGravity(Gravity.CENTER);
						fe.setTextColor(Color.WHITE);
						fe.setTypeface(null, Typeface.BOLD);
						tr.addView(fe);
						TextView ho=new TextView(Partidos2.this);
						ho.setText("Hora");
						ho.setGravity(Gravity.CENTER);
						ho.setTextColor(Color.WHITE);
						ho.setTypeface(null, Typeface.BOLD);
						tr.addView(ho);
						TextView ca=new TextView(Partidos2.this);
						ca.setText("Cat.");
						ca.setGravity(Gravity.CENTER);
						ca.setTextColor(Color.WHITE);
						ca.setTypeface(null, Typeface.BOLD);
						tr.addView(ca);
						TextView pi=new TextView(Partidos2.this);
						pi.setText("Pista");
						pi.setGravity(Gravity.CENTER);
						pi.setTextColor(Color.WHITE);
						pi.setTypeface(null, Typeface.BOLD);
						tr.addView(pi);
						tv.addView(tr);
						final View vline=new View(Partidos2.this);
						vline.setLayoutParams(new TableRow.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 2));
						vline.setBackgroundColor(Color.WHITE);
						tv.addView(vline);
						contador=0;
					}
					else{	//Si el contador no es uno imprime los datos de la tabla
						JSONObject json_data=jArray.getJSONObject(i);
						TextView b=new TextView(Partidos2.this);
						String stime=String.valueOf(json_data.getString("EquipoA"));
						b.setText(stime);
						b.setGravity(Gravity.CENTER);
						b.setTextColor(Color.WHITE);
						b.setPadding(0, 0, 10, 0);
						tr.addView(b);
						TextView b1=new TextView(Partidos2.this);
						String stime1=String.valueOf(json_data.getString("EquipoB"));
						b1.setText(stime1);
						b1.setGravity(Gravity.CENTER);
						b1.setTextColor(Color.WHITE);
						b1.setPadding(0, 0, 10, 0);
						tr.addView(b1);
						TextView b2=new TextView(Partidos2.this);
						String stime2=String.valueOf(json_data.getString("Fecha"));
						b2.setText(stime2);
						b2.setGravity(Gravity.CENTER);
						b2.setTextColor(Color.WHITE);
						b2.setPadding(0, 0, 10, 0);
						tr.addView(b2);
						TextView b3=new TextView(Partidos2.this);
						String stime3=String.valueOf(json_data.getString("Hora"));
						b3.setText(stime3);
						b3.setGravity(Gravity.CENTER);
						b3.setTextColor(Color.WHITE);
						b3.setPadding(0, 0, 10, 0);
						tr.addView(b3);
						TextView b4=new TextView(Partidos2.this);
						String stime4=String.valueOf(json_data.getString("Abreviatura"));
						b4.setText(stime4);
						b4.setGravity(Gravity.CENTER);
						b4.setTextColor(Color.WHITE);
						b4.setPadding(0, 0, 10, 0);
						tr.addView(b4);
						TextView b5=new TextView(Partidos2.this);
						String stime5=String.valueOf(json_data.getString("Pista"));
						b5.setText(stime5);
						b5.setGravity(Gravity.CENTER);
						b5.setTextColor(Color.WHITE);
						tr.addView(b5);
						tv.addView(tr);
						final View vline1=new View(Partidos2.this);
						vline1.setLayoutParams(new TableRow.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1));
						vline1.setBackgroundColor(Color.WHITE);
						tv.addView(vline1);
						pDialog.dismiss();
					}
				}
			} catch(JSONException e){
				Log.e("Error: ", e.toString());
			}
		}
	}
}
