package com.example.arbitrosfabmalaga;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Telefonos extends Activity{	//Clase para cargar listado de telefonos y guardarlos en la agenda
	String opcion;	//Variable para la opci�n seleccionada
	ListView listArbitros;	//Lista de �rbitros
	Button btnAgregar;	//Bot�n para agregar
	String nombre;	//Variable para el nombre del contacto
	String movil;	//Variable para el n�mero de m�vil
	String fijo;	//Variable para el n�mero fijo 
	String email;	//Variable para el email
	private String result=null;	//Variable para el resultado
	private InputStream is=null;	//Variable para el lector de datos
	private StringBuilder sb=null;	//Variable para construir cadenas
	@Override
	protected void onCreate(Bundle savedInstanceState) {	//M�todo para crear la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	//Crea la instancia
		setContentView(R.layout.activity_telefonos);	//Asigna el layout de la activity
		btnAgregar=(Button)findViewById(R.id.btnAgregar);	//Referencia el bot�n de agregar
		listArbitros=(ListView)findViewById(R.id.listArbitros);	//Referencia la lista de �rbitros
		btnAgregar.setPadding(10, 10, 10, 10);	//Asigna padding al bot�n
		TareaContactos tarea=new TareaContactos();	//Nuevo objeto de la clase TareaContactos
		tarea.cargaContenido(getApplicationContext());	//Carga el contexto
		tarea.onPreExecute();	//Ejecuta m�todo previo a la ejecuci�n en background
		tarea.execute(listArbitros);	//Ejecuta la clase
		listArbitros.setOnItemClickListener(new OnItemClickListener(){	//Evento onclick en un elemento de la lista
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position, long id){	//M�todo del evento onclick de la lista
		        opcion=((Arbitros)a.getAdapter().getItem(position)).getNombre();	//Asigna a la variable el elemento seleccionado
		        btnAgregar.setOnClickListener(new OnClickListener(){	//Evento onclic del bot�n
					@Override
					public void onClick(View v){	//M�todo del evento onclick del bot�n
						new asyncarbitros().execute(opcion); 	//Ejecuta la clase asyncarbitros
						Toast.makeText(getApplicationContext(), "Se agreg� el contacto a la agenda del tel�fono", Toast.LENGTH_LONG).show();	//Mensaje que indica que se agreg� el contacto
				    }
				});
		    } 
		});	
	}
	static class TareaContactos extends AsyncTask<ListView, Void, ArrayAdapter<Arbitros>>{	//Clase asincrona para obtener los �rbitros
		Context contexto;	//Contexto
		ListView listArbitros;	//Lista de �rbitros
		InputStream is;	//Lector
		ArrayList<Arbitros> lista=new ArrayList<Arbitros>();	//Arraylist de Arbitros
		public void cargaContenido(Context contexto){	//Constructor
			this.contexto=contexto;
		}
		@Override
		protected void onPreExecute(){	//M�todo previo a la ejecuci�n en bckground
			
		}
		@Override
		protected ArrayAdapter<Arbitros> doInBackground(ListView... params){	//M�todo background para obtener la lista de �rbitros
			listArbitros=params[0];	//Lista de �rbitros
			String resultado="error";	//Variable para el resultado
			Arbitros arb;	//Variable para los �rbitros
			HttpClient cliente=new DefaultHttpClient();	//Cliente HTTP
			HttpGet peticion=new HttpGet("http://www.abelsuviri.tk/android/arbitros.php");	//Script PHP para cargar los �rbitros
			try{
				HttpResponse response=cliente.execute(peticion);	//Ejecuta la petici�n al servidor
				HttpEntity contenido=response.getEntity();	//Obtiene la respuesta del servidor
				is=contenido.getContent();	//Lee el contenido de la respuesta
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			BufferedReader br=new BufferedReader(new InputStreamReader(is));	//Leector
			StringBuilder sb=new StringBuilder();	//Creador de cadenas
			String linea=null;	//Variable para las lineas
			try{
				while((linea=br.readLine())!=null){	//Mientras haya datos sigue leyendo y los agrega al creador de cadenas
					sb.append(linea);
				}
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			try{
				is.close();	//Cierra el lector
			} catch(IOException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			resultado=sb.toString();	//Guarda los datos en la variable resultado
			try{
				JSONArray jArray=new JSONArray(resultado);	//Crea un array JSON
				for(int i=0;i<jArray.length();i++){	
					JSONObject objetoJSON=jArray.getJSONObject(i);	//Lee los datos devueltos
					arb=new Arbitros(objetoJSON.getString("nombre"));	//Obtiene el campo de la BD y lo guarda en la variable arb
					lista.add(arb);	//A�ade los �rbitros a la lista
				}
			} catch(JSONException e){
				Toast.makeText(contexto , e.toString(), Toast.LENGTH_SHORT).show();
			}
			ArrayAdapter<Arbitros> adaptador=new ArrayAdapter<Arbitros>(contexto, android.R.layout.simple_list_item_1, lista);	//Adaptador de la lista
			return adaptador;
		}
		@Override
		protected void onPostExecute(ArrayAdapter<Arbitros> result){	//M�todo posterior a la ejecuci�n en background
			listArbitros.setAdapter(result);	//Asigna el adaptador
		}
		@Override
		protected void onProgressUpdate(Void... values){
			
		}
	}
	class asyncarbitros extends AsyncTask<String, String, String>{	//Clase asincrona para guardar el contacto
		@Override
		protected String doInBackground(String... params){	//M�todo en background para leer de la BD
			String ok="ok";	//Variable a devolver porque el m�todo debe devolver una cadena
			try{
				HttpClient httpclient=new DefaultHttpClient();	//Cliente HTTP
				HttpPost httppost=new HttpPost("http://abelsuviri.tk/android/contacto.php?nombre="+URLEncoder.encode(opcion, "UTF-8"));	//Script para la consulta en la base de datos
				HttpResponse response=httpclient.execute(httppost);	//Ejecuta la petici�n al servidor
				HttpEntity entity=response.getEntity();	//Obtiene la respuesta del servidor
				is=entity.getContent();	//Lee la respuesta
			} catch(Exception e){
				Log.e("Conexion: ",e.toString());
			}
			try{
				BufferedReader reader=new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);	//Lector
				sb=new StringBuilder();	//Creador de cadenas
				sb.append(reader.readLine()+"\n");	
				String line="0";	
				while((line=reader.readLine())!=null){	//Mientras haya lineas las lee y las asigna al creador de cadenas
					sb.append(line+"\n");
				}
				is.close();	//Cierra el lector
				result=sb.toString();	//Almacena el resultado en la variable
			} catch(Exception e){
				Log.e("Error: ", e.toString());
			}
			try{
				JSONArray jArray=new JSONArray(result);	//Array JSON
				for(int i=0;i<jArray.length();i++){	
					JSONObject json_data=jArray.getJSONObject(i);	//Obtiene los datos del array
					nombre=String.valueOf(json_data.getString("Nom"));	//Asigna el campo nombre
					fijo=String.valueOf(json_data.getString("Fijo"));	//Asigna el campo fijo
					movil=String.valueOf(json_data.getString("Movil"));	//Asigna el campo movil
					email=String.valueOf(json_data.get("Email"));	//Asigna el campo email
				}
				ArrayList < ContentProviderOperation > ops = new ArrayList < ContentProviderOperation > ();	//Arraylist del ContentProvider
				 ops.add(ContentProviderOperation.newInsert(	//A�ade un operador de content provider de inserci�n
				 ContactsContract.RawContacts.CONTENT_URI)	//Content de contactos
				     .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)	//Tipo de cuenta
				     .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)	//Tipo de nombre
				     .build());	//Crea
				 if (nombre != null) {	//Si el nombre no es nulo
				     ops.add(ContentProviderOperation.newInsert(	//A�ade al content provider el nombre
				     ContactsContract.Data.CONTENT_URI)	
				         .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				         .withValue(ContactsContract.Data.MIMETYPE,
				     ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
				         .withValue(
				     ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
				     nombre).build());
				 }                     
				 if (movil != null) {	//Si el movil no es nulo
				     ops.add(ContentProviderOperation.	//A�ade al content provider el movil
				     newInsert(ContactsContract.Data.CONTENT_URI)
				         .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				         .withValue(ContactsContract.Data.MIMETYPE,
				     ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				         .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, movil)
				         .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
				     ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
				         .build());
				 }
				 if (fijo != null) {	//Si el fijo no es nulo
				     ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)	//A�ade al content provider el fijo
				         .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				         .withValue(ContactsContract.Data.MIMETYPE,
				     ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				         .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, fijo)
				         .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
				     ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
				         .build());
				 }
				 if (email != null) {	//Si el email no es nulo
				     ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)	//A�ade al content provider el email
				         .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				         .withValue(ContactsContract.Data.MIMETYPE,
				     ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
				         .withValue(ContactsContract.CommonDataKinds.Email.DATA, email)
				         .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
				         .build());
				 }
				 try {
				     getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);	//A�ade los datos del content provider a la agenda del tel�fono
				 } catch (Exception e) {
				     Log.e("Error: ",e.getMessage());
				 } 
			} catch(JSONException e){
				Log.e("Error: ", e.toString());
			}
			return ok;
		}
	}
}
