package com.example.arbitrosfabmalaga;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;


public class MainActivity extends Activity {	//Clase de la actividad principal

    @Override
    protected void onCreate(Bundle savedInstanceState) {	//M�todo para la creaci�n de la activity
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
        super.onCreate(savedInstanceState);	//Crea la instancia
        setContentView(R.layout.activity_main);	//Asigna el layout de la activity
        //Referencia de los tres botones de la activity
        final Button btnIniciarSesion=(Button)findViewById(R.id.btnIniciarSesion);
        final Button btnPartidos=(Button)findViewById(R.id.btnPartidos);	
        final Button btnPistas=(Button)findViewById(R.id.btnPistas);
        btnIniciarSesion.setOnClickListener(new OnClickListener(){	//Evento onclick del primer bot�n
        	@Override
			public void onClick(View v){		//M�todo del evento onclick
        		Intent intent=new Intent(MainActivity.this, InicioSesion.class);	//Crea un nuveo lanzador para acceder a la activity de inicio de sesi�n
        		startActivity(intent);	//Inicia la activity
        	}
        });
        btnPartidos.setOnClickListener(new OnClickListener(){	//Evento onclick del segundo bot�n
        	@Override
			public void onClick(View v){	//M�todo del evento onclick
        		Intent intent=new Intent(MainActivity.this, Partidos.class);	//Crea un nuevo lanzador para acceder a la activity partidos
        		startActivity(intent);	//Inicia la activity
        	}
        });
        btnPistas.setOnClickListener(new OnClickListener(){		//Evento onclick del tercr bot�n
        	@Override
			public void onClick(View v){	//M�todo del evento onclick
        		Intent intent=new Intent(MainActivity.this, Pistas.class);	//Crea un nuevo lanzador para acceder a la activity pistas
        		startActivity(intent);	//Inicia la activity
        	}
        });
    }
}
