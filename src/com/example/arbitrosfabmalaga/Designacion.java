package com.example.arbitrosfabmalaga;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class Designacion extends Activity {	//Clase principal de la activity
	@Override
	protected void onCreate(Bundle savedInstanceState) {	//M�todo de creaci�n de la activity
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	//Oculta la barra de t�tulo
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_designacion);	//Asigna el layout de la activity
		Bundle bundle = this.getIntent().getExtras();	//Lee los par�metros que se mandaron
		final String usuario=bundle.getString("usuario");	//Obtiene el par�metro usuario de la activity anterior
		final String direccion="http://abelsuviri.tk/android/designacion.php?usuario="+usuario;	//Direcci�n del servidor
		Button btnConfirmarDesignacion=(Button)findViewById(R.id.btnConfirmarDesignacion);	//Referencia el bot�n
		btnConfirmarDesignacion.setPadding(10, 10, 10, 10);	//Asigna padding al bot�n
		WebView wb=(WebView)findViewById(R.id.wbVerDesignacion);	//Referencia el webview
		wb.getSettings().setJavaScriptEnabled(true);	//Activa JavaScript en el webview
		wb.setWebViewClient(new Callback());	//Asigna el cliente del webview
		wb.loadUrl("http://docs.google.com/gview?embedded=true&url="+direccion);	//Carga la url que mostrar� el PDF de la designaci�n
		btnConfirmarDesignacion.setOnClickListener(new OnClickListener(){	//Evento click del bot�n
			@Override
			public void onClick(View v){	//M�todo del click
				String[] para={"abelsuviri@gmail.com"};	//Variable de destinatario
				String mensaje="El �rbitro "+usuario+" ha confirmado su designaci�n.";	//Variable del mensaje a enviar
				Intent email=new Intent(Intent.ACTION_SEND);	//Creaci�n del lanzador nuevo como envio de email
				email.putExtra(Intent.EXTRA_EMAIL, para);	//Asigna el destinatario
				email.putExtra(Intent.EXTRA_SUBJECT, "Confirmaci�n de designaci�n");	//Asigna el asunto
				email.putExtra(Intent.EXTRA_TEXT, mensaje);	//Asigna el mensaje
				email.setType("message/rfc822");	//Asigna el charset
				startActivity(Intent.createChooser(email, "Email "));	//Lanza el cliente de correo electr�nico
			}
		});
	}
	private class Callback extends WebViewClient {	//Clase para el cliente del webview
        @Override
        public boolean shouldOverrideUrlLoading(	//M�todo del cliente del webview
                WebView view, String url) {
            return(false);
        }
    }
}
