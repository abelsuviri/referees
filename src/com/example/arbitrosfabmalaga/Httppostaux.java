package com.example.arbitrosfabmalaga;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import android.util.Log;

public class Httppostaux{  	//Clase para el auxiliar de HTTP Post
	InputStream is = null;	//Asigna la entrada a null
	String result = "";	//Variable resultado vacion
	public JSONArray getserverdata(ArrayList<NameValuePair> parameters, String urlwebserver ){	//Metodo para obtener un array JSON
		httppostconnect(parameters,urlwebserver);	//Conecta al servidor
		if (is!=null){	//Si hay conexi�n ejecuta el m�todo getresponse
			getpostresponse();
			return getjsonarray();
		}
		else{
			return null;
		}
	}
	private void httppostconnect(ArrayList<NameValuePair> parametros, String urlwebserver){ //M�todo para conectarse al servidor
	   try{
		   HttpClient httpclient = new DefaultHttpClient();	//Asigna el cliente HTTP
		   HttpPost httppost = new HttpPost(urlwebserver);	//Asigna el envio de HTTP
		   httppost.setEntity(new UrlEncodedFormEntity(parametros));	//Asigna la entidad
		   HttpResponse response = httpclient.execute(httppost);	//Comprueba si hay respuesta
		   HttpEntity entity = response.getEntity();	//Obtiene la entidad
		   is = entity.getContent();	//Obtiene el contenido
	   } catch(Exception e){
		   Log.e("log_tag", "Error in http connection "+e.toString());
	   }
	}
	public void getpostresponse(){	//M�todo para obtener los datos
	   try{
		   BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);	//Lector
		   StringBuilder sb = new StringBuilder();	//Creador de cadenas
		   String line = null;	//Variable para las l�neas
		   while ((line = reader.readLine()) != null) {	//Mientras haya lineas sigue leyendo
			   sb.append(line + "\n");
		   }
		   is.close();	//Cierra el lector
		   result=sb.toString();	//Asigna al resultado el creador de cadenas
		   Log.e("getpostresponse"," result= "+sb.toString());
	   } catch(Exception e){
		   Log.e("log_tag", "Error converting result "+e.toString());
	   }
	}
	  
	public JSONArray getjsonarray(){	//M�todo para crea el array JSON
	   try{
		   JSONArray jArray = new JSONArray(result); //Crea el array y le asigna los valores obtenidos de la lectura
		   return jArray;
	   } catch(JSONException e){
		   Log.e("log_tag", "Error parsing data "+e.toString());
		   return null;
	   }
	
	} 
}	
